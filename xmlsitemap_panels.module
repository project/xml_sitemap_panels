<?php
/**
 * @file
 * Module file for xmlsitemap_panels.
 */

/**
 * Implements hook_permission().
 */
function xmlsitemap_panels_permission() {
  return array(
    'manage page_manager xmlsitemap_panels' => array(
      'title' => t('Administer XML Sitemap settings of Panels pages'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_cron().
 *
 * Process pages not found in the {xmlsitemap} table.
 */
function xmlsitemap_panels_cron() {
  // Panels are not added/updated frequently, to reduce load we process in cron
  // only if cron regeneration is enabled for xmlsitemap.
  if (variable_get('xmlsitemap_disable_cron_regeneration', 0)) {
    xmlsitemap_panels_xmlsitemap_index_links();
  }
}

/**
 * Implements hook_xmlsitemap_index_links().
 */
function xmlsitemap_panels_xmlsitemap_index_links() {
  // Load the appropriate pages.
  ctools_include('page', 'page_manager', 'plugins/tasks');
  $task = page_manager_get_task('page');

  // Loop through all pages.
  foreach (page_manager_get_task_subtasks($task) as $subtask_id => $subtask) {
    // Load the handler (from cache or code).
    $handlers = page_manager_load_sorted_handlers($task, $subtask_id, TRUE);

    // If no handle available, we continue.
    if (empty($handlers)) {
      continue;
    }

    // We get the first matching handler, we will have only one as we passed the
    // subtask_id.
    $handler = reset($handlers);

    // We process only if xmlsitemap is enabled for this page.
    if (isset($handler->conf['xmlsitemap_enabled']) && $handler->conf['xmlsitemap_enabled'] == 1) {
      // Get the path for current handler.
      $path = _xmlsitemap_panels_panel_page_path($handler->subtask);

      // Prepare condition to check if page exists.
      $conditions = array(
        'loc' => $path,
        'type' => 'panel',
      );

      // Check first if link is not already added.
      if (xmlsitemap_link_load_multiple($conditions)) {
        // If disabled and already added, we delete the link.
        if (!empty($subtask['disabled'])) {
          _xmlsitemap_panels_delete_link($path);
        }

        // Already added, we continue to next page.
        continue;
      }

      // If it is disabled, we don't need to add to xmlsitemap.
      if (!empty($subtask['disabled'])) {
        // We continue to next page.
        continue;
      }

      $link = array(
        'id' => db_query('SELECT MAX(id) FROM {xmlsitemap} WHERE type = :type', array(':type' => 'panel'))->fetchField() + 1,
        'loc' => $path,
        'priority' => $handler->conf['priority'],
        'lastmod' => 0,
        'changefreq' => $handler->conf['changefreq'],
        'changecount' => 0,
        'language' => $handler->conf['language'],
        'type' => 'panel',
      );

      xmlsitemap_link_save($link);
    }
  }
}

/**
 * Implements hook_page_manager_variant_operations_alter().
 *
 * Adds a custom administrative interface per variant for adding
 * panel pages to xmlsitemaps.
 */
function xmlsitemap_panels_page_manager_variant_operations_alter(&$operations, &$handler) {
  if ($handler->task == 'page' && user_access('manage page_manager xmlsitemap_panels')) {
    $keys = array_keys($operations['children']);
    $before = array_slice($operations['children'], 0, array_search('settings', $keys) + 1);
    $after = array_slice($operations['children'], array_search('settings', $keys) + 1);
    $xmlsitemap = array(
      'xmlsitemap' => array(
        'title' => t('XML sitemap'),
        'description' => t('Allow a given variant to provide a xmlsitemap.'),
        'form' => 'xmlsitemap_panels_subtask_form',
      ),
    );
    $operations['children'] = array_merge($before, $xmlsitemap, $after);
  }
}

/**
 * Form callback for the administrative interface to add path aliases.
 */
function xmlsitemap_panels_subtask_form($form, $form_state) {
  module_load_include('inc', 'xmlsitemap', 'xmlsitemap.admin');
  $conf = $form_state['handler']->conf;

  $form['settings']['xmlsitemap_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable XML sitemap.'),
    '#default_value' => isset($conf['xmlsitemap_enabled']) ? $conf['xmlsitemap_enabled'] : FALSE,
  );

  $form['xmlsitemap'] = array(
    '#title' => t('Settings'),
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="xmlsitemap_enabled"]' => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );

  $form['xmlsitemap']['priority'] = array(
    '#type' => 'select',
    '#title' => t('Priority'),
    '#options' => xmlsitemap_get_priority_options(),
    '#default_value' => isset($conf['priority']) ? number_format($conf['priority'], 1) : 0,
    '#description' => t('The priority of this URL relative to other URLs on your site.'),
  );

  $form['xmlsitemap']['changefreq'] = array(
    '#type' => 'select',
    '#title' => t('Change frequency'),
    '#options' => array(0 => t('None')) + xmlsitemap_get_changefreq_options(),
    '#default_value' => isset($conf['changefreq']) ? $conf['changefreq'] : 0,
    '#description' => t('How frequently the page is likely to change. This value provides general information to search engines and may not correlate exactly to how often they crawl the page.'),
  );

  $languages = module_exists('locale') ? locale_language_list() : array();

  $form['xmlsitemap']['language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#default_value' => isset($conf['language']) ? $conf['language'] : LANGUAGE_NONE,
    '#options' => array(LANGUAGE_NONE => t('Language neutral')) + $languages,
    '#access' => $languages,
  );

  return $form;
}

/**
 * Form submit callback.
 */
function xmlsitemap_panels_subtask_form_submit($form, &$form_state) {
  $form_state['handler']->conf['xmlsitemap_enabled'] = $form_state['values']['xmlsitemap_enabled'];
  $form_state['handler']->conf['priority'] = $form_state['values']['priority'];
  $form_state['handler']->conf['changefreq'] = $form_state['values']['changefreq'];
  $form_state['handler']->conf['language'] = $form_state['values']['language'];

  $conf = $form_state['handler']->conf;
  $path = $form_state['page']->subtask['subtask']->path;

  // We don't handle dynamic aliases as of now.
  if (strpos($path, '%') > -1) {
    return;
  }

  _xmlsitemap_panels_add_link($path, $conf);
}

/**
 * Helper function to delete and add link again to xmlsitemap if enabled.
 */
function _xmlsitemap_panels_add_link($path, $conf) {
  // Delete the link if exists, we create it every-time.
  _xmlsitemap_panels_delete_link($path);

  // Don't create the link if it isn't enabled.
  if (empty($conf['xmlsitemap_enabled'])) {
    variable_set('xmlsitemap_regenerate_needed', TRUE);
    return;
  }

  $link = array(
    'id' => db_query('SELECT MAX(id) FROM {xmlsitemap} WHERE type = :type', array(':type' => 'panel'))->fetchField() + 1,
    'loc' => $path,
    'priority' => $conf['priority'],
    'lastmod' => 0,
    'changefreq' => $conf['changefreq'],
    'changecount' => 0,
    'language' => $conf['language'],
    'type' => 'panel',
  );

  xmlsitemap_link_save($link);
}

/**
 * Check if url already in xmlsitemap table.
 */
function _xmlsitemap_panels_delete_link($loc) {
  $query = db_delete('xmlsitemap');
  $query->condition('type', 'panel');
  $query->condition('loc', $loc);
  $query->execute();
}

/**
 * The function returns the path of the panel page.
 *
 * @param string $page_id
 *   The panel page id.
 */
function _xmlsitemap_panels_panel_page_path($page_id) {
  $path = '';

  ctools_include('export');
  $result = ctools_export_load_object('page_manager_pages', 'names', array($page_id));

  if (!empty($result) && !empty($result[$page_id])) {
    $path = $result[$page_id]->path;
  }

  return $path;
}
